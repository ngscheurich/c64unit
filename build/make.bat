
REM Delete output bin folder
del /q ..\bin\*.*

REM Create c64unit package and create symbols list
64tass.exe -b -a "pages\core1000.asm" -o "..\bin\core1000.bin" -l  "..\bin\symbols.asm
@echo off
if %errorlevel% neq 0 (
	exit /b %errorlevel%
)
@echo on

REM Compile package for set of memory areas
64tass.exe -b -a "pages\core2000.asm" -o "..\bin\core2000.bin"
64tass.exe -b -a "pages\core3000.asm" -o "..\bin\core3000.bin"
64tass.exe -b -a "pages\core4000.asm" -o "..\bin\core4000.bin"
64tass.exe -b -a "pages\core5000.asm" -o "..\bin\core5000.bin"
64tass.exe -b -a "pages\core6000.asm" -o "..\bin\core6000.bin"
64tass.exe -b -a "pages\core7000.asm" -o "..\bin\core7000.bin"
64tass.exe -b -a "pages\core8000.asm" -o "..\bin\core8000.bin"
64tass.exe -b -a "pages\core9000.asm" -o "..\bin\core9000.bin"
64tass.exe -b -a "pages\coreA000.asm" -o "..\bin\coreA000.bin"
64tass.exe -b -a "pages\coreB000.asm" -o "..\bin\coreB000.bin"
64tass.exe -b -a "pages\coreC000.asm" -o "..\bin\coreC000.bin"
64tass.exe -b -a "pages\coreD000.asm" -o "..\bin\coreD000.bin"
64tass.exe -b -a "pages\coreE000.asm" -o "..\bin\coreE000.bin"
64tass.exe -b -a "pages\coreF000.asm" -o "..\bin\coreF000.bin"

REM Run Symlinker to generate symbols list and core package installers for each type of cross-assembler
php c64unitSymlinker\run.php
