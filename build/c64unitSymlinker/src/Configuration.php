<?php

namespace Commocore\C64Unit\Symlinker;

use InvalidArgumentException;

class Configuration
{
    const TASS = '64tass';

    const KICK_ASSEMBLER = 'KickAssembler';

    const DASM = 'DASM';

    const ACME = 'ACME';

    const CA65 = 'ca65';

    const XA65 = 'xa65';

    private static $outputDirectory = array(
        self::TASS => '64tass',
        self::KICK_ASSEMBLER => 'kick-assembler',
        self::DASM => 'dasm',
        self::ACME => 'acme',
        self::CA65 => 'ca65',
        self::XA65 => 'xa65'
    );

    /**
     * @return array
     */
    public static function getSupportedCrossAssemblers()
    {
        return array(
            self::TASS,
            self::DASM,
            self::KICK_ASSEMBLER,
            self::ACME,
            self::CA65,
            self::XA65
        );
    }

    /**
     * @param string $crossAssemblerName
     * @return string
     */
    public static function getOutputDirectory($crossAssemblerName)
    {
        if (!in_array($crossAssemblerName, self::getSupportedCrossAssemblers())) {
            throw new InvalidArgumentException('Cross-assembler ' . $crossAssemblerName . ' not supported');
        }
        return self::$outputDirectory[$crossAssemblerName];
    }

    /**
     * @return array
     */
    public function getCorePages()
    {
        return array(
            '1000',
            '2000',
            '3000',
            '4000',
            '5000',
			'6000',
			'7000',
			'8000',
			'9000',
			'A000',
			'B000',
			'C000',
            'D000',
            'E000',
            'F000'
        );
    }
}
