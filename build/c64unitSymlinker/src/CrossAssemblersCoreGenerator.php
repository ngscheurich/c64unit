<?php

namespace Commocore\C64Unit\Symlinker;

use Commocore\C64Unit\Symlinker\CoreInstallers\CoreInstallerFactory;
use Commocore\C64Unit\Symlinker\IO\CoreInstallerWriter;

class CrossAssemblersCoreGenerator
{
    /**
     * @var CoreInstallerFactory
     */
    private $factory;

    /**
     * @var CoreInstallerWriter
     */
    private $writer;

    public function __construct(CoreInstallerFactory $coreInstallerFactory, CoreInstallerWriter $coreInstallerWriter)
    {
        $this->factory = $coreInstallerFactory;
        $this->writer = $coreInstallerWriter;
    }

    public function generate($crossAssembler)
    {
        $this->writer->save($this->factory->get($crossAssembler));
    }
}
