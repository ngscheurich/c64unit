<?php

namespace Commocore\C64Unit\Symlinker\CoreInstallers;

use Commocore\C64Unit\Symlinker\Configuration;
use Commocore\C64Unit\Symlinker\CoreInstallers\Strategy\AcmeCoreInstaller;
use Commocore\C64Unit\Symlinker\CoreInstallers\Strategy\Ca65CoreInstaller;
use Commocore\C64Unit\Symlinker\CoreInstallers\Strategy\DasmCoreInstaller;
use Commocore\C64Unit\Symlinker\CoreInstallers\Strategy\KickAssemblerCoreInstaller;
use Commocore\C64Unit\Symlinker\CoreInstallers\Strategy\TassCoreInstaller;
use Commocore\C64Unit\Symlinker\CoreInstallers\Strategy\Xa65CoreInstaller;
use InvalidArgumentException;

class CoreInstallerFactory
{
    public function get($crossAssemblerName)
    {
        switch ($crossAssemblerName) {
            case Configuration::TASS:
                return new TassCoreInstaller();
            case Configuration::KICK_ASSEMBLER:
                return new KickAssemblerCoreInstaller();
            case Configuration::DASM:
                return new DasmCoreInstaller();
            case Configuration::ACME:
                return new AcmeCoreInstaller();
            case Configuration::CA65:
                return new Ca65CoreInstaller();
            case Configuration::XA65:
                return new Xa65CoreInstaller();
            default:
                throw new InvalidArgumentException('Cross assembler not implemented');
        }
    }
}
