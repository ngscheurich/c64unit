<?php

namespace Commocore\C64Unit\Symlinker\CoreInstallers\Strategy;

interface CoreInstallerInterface
{
    public function getName();

    public function getContent($page);
}
