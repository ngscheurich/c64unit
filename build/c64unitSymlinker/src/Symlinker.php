<?php

namespace Commocore\C64Unit\Symlinker;

use Commocore\C64Unit\Symlinker\IO\SymlinkWriter;
use Commocore\C64Unit\Symlinker\Symbols\SymbolsFactory;
use Commocore\C64Unit\Symlinker\Symbols\Strategy\SymbolsInterface;

class Symlinker
{
    /**
     * @var SymbolsFactory
     */
    private $strategyFactory;

    /**
     * @var SymlinkWriter
     */
    private $symlinkWriter;

    /**
     * @param SymbolsFactory $strategyFactory
     * @param SymlinkWriter $symlinkWriter
     */
    public function __construct(
        SymbolsFactory $strategyFactory,
        SymlinkWriter $symlinkWriter
    ) {
        $this->strategyFactory = $strategyFactory;
        $this->symlinkWriter = $symlinkWriter;
    }

    /**
     * @param string $crossAssemblerName
     */
    public function save($crossAssemblerName)
    {
        $strategy = $this->getSymbolsStrategy($crossAssemblerName);
        $this->symlinkWriter->save($strategy);
    }

    /**
     * @param string $crossAssemblerName
     */
    public function display($crossAssemblerName)
    {
        $strategy = $this->getSymbolsStrategy($crossAssemblerName);
        $symbolsList = $strategy->getSymbolsList();
        foreach ($symbolsList->getAll() as $value) {
            echo $value . PHP_EOL;
        }
    }

    /**
     * @param string $crossAssemblerName
     * @return SymbolsInterface
     */
    private function getSymbolsStrategy($crossAssemblerName)
    {
        return $this->strategyFactory->get($crossAssemblerName);
    }
}
