<?php

namespace Commocore\C64Unit\Symlinker\Symbols\Strategy;

use Commocore\C64Unit\Symlinker\Configuration;
use Commocore\C64Unit\Symlinker\Symbols\SymbolsList;

class DasmSymbols extends DefaultSymbols implements SymbolsInterface
{
    /**
     * @return SymbolsList
     */
    public function getSymbolsList()
    {
        $symbolsList = new SymbolsList();
        $values = $this->getSymlinkReader()->getData();
        foreach ($values as $value) {
            $symbolsList->add($value['key'] . ' equ ' . $value['address']);
        }
        return $symbolsList;
    }

    /**
     * @return string
     */
    public function getOutputDirectory()
    {
        return Configuration::getOutputDirectory(Configuration::DASM);
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return '; This file is automatically generated by Symlinker';
    }
}
