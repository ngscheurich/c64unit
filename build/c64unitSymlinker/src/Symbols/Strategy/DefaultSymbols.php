<?php

namespace Commocore\C64Unit\Symlinker\Symbols\Strategy;

use Commocore\C64Unit\Symlinker\IO\SymlinkReader;

abstract class DefaultSymbols
{
    /**
     * @var SymlinkReader
     */
    protected $symlinkReader;

    public function __construct(SymlinkReader $symlinkReader)
    {
        $this->symlinkReader = $symlinkReader;
    }

    /**
     * @return SymlinkReader
     */
    protected function getSymlinkReader()
    {
        return $this->symlinkReader;
    }

    /**
     * @return string
     */
    public function getOutputDirectory()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return '';
    }
}
