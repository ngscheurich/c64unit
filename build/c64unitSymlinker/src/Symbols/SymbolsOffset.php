<?php

namespace Commocore\C64Unit\Symlinker\Symbols;

class SymbolsOffset
{
    const LOGICAL_ADDRESS_OFFSET = ' - $1000';

    /**
     * @return string
     */
    public static function get()
    {
        return self::LOGICAL_ADDRESS_OFFSET . ' + coreBinaryLocation';
    }
}
