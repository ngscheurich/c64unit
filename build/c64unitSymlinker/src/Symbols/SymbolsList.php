<?php

namespace Commocore\C64Unit\Symlinker\Symbols;

use InvalidArgumentException;

class SymbolsList
{
    /**
     * @var array
     */
    private $data;

    public function __construct()
    {
        $this->data = array();
    }

    /**
     * @param string $value
     * @throws InvalidArgumentException
     */
    public function add($value)
    {
        if (!is_string($value)) {
            throw new InvalidArgumentException('Wrong value type');
        }
        $this->data[] = $value . SymbolsOffset::get();
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->data;
    }
}
