<?php

namespace Commocore\C64Unit\Symlinker\Symbols;

use Commocore\C64Unit\Symlinker\Configuration;
use Commocore\C64Unit\Symlinker\IO\SymlinkReader;
use Commocore\C64Unit\Symlinker\Symbols\Strategy\AcmeSymbols;
use Commocore\C64Unit\Symlinker\Symbols\Strategy\Ca65Symbols;
use Commocore\C64Unit\Symlinker\Symbols\Strategy\SymbolsInterface;
use Commocore\C64Unit\Symlinker\Symbols\Strategy\KickAssemblerSymbols;
use Commocore\C64Unit\Symlinker\Symbols\Strategy\TassSymbols;
use Commocore\C64Unit\Symlinker\Symbols\Strategy\DasmSymbols;
use Commocore\C64Unit\Symlinker\Symbols\Strategy\Xa65Symbols;
use InvalidArgumentException;

class SymbolsFactory
{
    /**
     * @param SymlinkReader $symlinkReader
     */
    public function __construct(SymlinkReader $symlinkReader)
    {
        $this->symlinkReader = $symlinkReader;
    }

    /**
     * @param string $crossAssemblerName
     * @return SymbolsInterface
     */
    public function get($crossAssemblerName)
    {
        switch ($crossAssemblerName) {
            case Configuration::TASS:
                return new TassSymbols($this->symlinkReader);
            case Configuration::KICK_ASSEMBLER:
                return new KickAssemblerSymbols($this->symlinkReader);
            case Configuration::DASM:
                return new DasmSymbols($this->symlinkReader);
            case Configuration::ACME:
                return new AcmeSymbols($this->symlinkReader);
            case Configuration::CA65:
                return new Ca65Symbols($this->symlinkReader);
            case Configuration::XA65:
                return new Xa65Symbols($this->symlinkReader);
            default:
                throw new InvalidArgumentException('Cross assembler not implemented');
        }
    }
}
