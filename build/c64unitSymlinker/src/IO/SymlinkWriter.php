<?php

namespace Commocore\C64Unit\Symlinker\IO;

use Commocore\C64Unit\Symlinker\Symbols\Strategy\SymbolsInterface;

class SymlinkWriter
{
    /**
     * @param SymbolsInterface $symbolsStrategy
     */
    public function save(SymbolsInterface $symbolsStrategy)
    {
        $symbolsList = $symbolsStrategy->getSymbolsList();
        $outputDirectory = $symbolsStrategy->getOutputDirectory();

        $fullOutputDirectory = '../../cross-assemblers/' . $outputDirectory;
        $filename = $fullOutputDirectory . '/symbols.asm';

        if (!file_exists($fullOutputDirectory)) {
            mkdir($fullOutputDirectory);
        }

        $fp = fopen($filename, "w") or die('Cannot open file to save');
        fputs($fp, $symbolsStrategy->getComment() . PHP_EOL . PHP_EOL);
        foreach ($symbolsList->getAll() as $value) {
            fputs($fp, $value . PHP_EOL);
        }
        fclose($fp);
        $this->displayMessage($filename);
    }

    /**
     * @param string $filename
     */
    private function displayMessage($filename)
    {
        echo 'Symlink saved in ' . $filename . PHP_EOL;
    }
}
