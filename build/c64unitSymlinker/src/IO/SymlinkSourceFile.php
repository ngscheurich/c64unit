<?php

namespace Commocore\C64Unit\Symlinker\IO;

class SymlinkSourceFile
{
    const FILENAME = '../../bin/symbols.asm';

    /**
     * @var resource
     */
    private $file;

    /**
     * @var bool
     */
    private $eof;

    /**
     * @return string
     */
    public function getLine()
    {
        if (!$this->isFileOpened()) {
            $this->openFile();
        }
        $line = fgets($this->file);
        if (!$line) {
            $this->closeFile();
        }
        return $line;
    }

    /**
     * @return bool
     */
    public function isEof()
    {
        return $this->eof === true;
    }

    /**
     * @return bool
     */
    private function isFileOpened()
    {
        return $this->file !== null;
    }

    private function openFile()
    {
        $this->file = fopen(self::FILENAME, "r") or die('Cannot open the file error');
    }

    private function setEof()
    {
        $this->eof = true;
    }

    private function closeFile()
    {
        fclose($this->file);
        $this->setEof();
    }
}
