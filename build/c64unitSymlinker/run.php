<?php

use Commocore\C64Unit\Symlinker\Symlinker;
use Commocore\C64Unit\Symlinker\IO\SymlinkSourceFile;
use Commocore\C64Unit\Symlinker\IO\SymlinkReader;
use Commocore\C64Unit\Symlinker\IO\SymlinkWriter;
use Commocore\C64Unit\Symlinker\Symbols\SymbolsFactory;
use Commocore\C64Unit\Symlinker\CrossAssemblersCoreGenerator;
use Commocore\C64Unit\Symlinker\CoreInstallers\CoreInstallerFactory;
use Commocore\C64Unit\Symlinker\IO\CoreInstallerWriter;
use Commocore\C64Unit\Symlinker\Configuration;

chdir(__DIR__);

require_once 'vendor/autoload.php';

$configuration = new Configuration();

$symlinkFile = new SymlinkSourceFile();
$symlinkReader = new SymlinkReader($symlinkFile);
$symlinkWriter = new SymlinkWriter();
$symbolsFactory = new SymbolsFactory($symlinkReader);

$coreInstallerFactory = new CoreInstallerFactory();
$coreInstallerWriter = new CoreInstallerWriter($configuration);

$symlinker = new Symlinker($symbolsFactory, $symlinkWriter);
$coreInstaller = new CrossAssemblersCoreGenerator($coreInstallerFactory, $coreInstallerWriter);

foreach ($configuration->getSupportedCrossAssemblers() as $crossAssembler) {
    $symlinker->save($crossAssembler);
    $coreInstaller->generate($crossAssembler);
}
