@echo off
IF EXIST vendor\c64unit (
    cd vendor\c64unit
    git pull http://Commocore@bitbucket.org/Commocore/c64unit.git master
    cd ..\..\
) ELSE (
    git clone --origin c64unit http://Commocore@bitbucket.org/Commocore/c64unit.git vendor/c64unit
)
@echo on
