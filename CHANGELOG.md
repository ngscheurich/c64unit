﻿# Changelog

## [0.71] - 2018-12-17
### Changed
- `assertNegativeFlagSet` has been split into two: `assertNegativeFlagSignedSet` and `assertNegativeFlagUnsignedSet`
- `assertNegativeFlagNotSet` has been split into two: `assertNegativeFlagSignedNotSet` and `assertNegativeFlagUnsignedNotSet`

### Removed
- `assertNegativeFlagSet` and `assertNegativeFlagNotSet`


## [0.7] - 2018-08-29
### Added
- `loadDataSet` macro which loads a byte under particular address
- This CHANGELOG file, finally!

### Changed
- `getDataSet` macro renamed to `loadDataSetToA` which loads a byte to accumulator
- `getDataSetWord` macro renamed to `loadDataSetWord` which loads a word under particular address
- `getDataSetWordToLoHi` macro renamed to `loadDataSetWordToLoHi` which loads a word under two different address locations (lo, hi)


## [0.65] - 2018-08-12
### Added
- `getDataSetWordToLoHi` macro which loads a word the same way as for `getDataSetWord` but under two different address locations (lo, hi)


## [0.64] - 2018-08-08
### Added
- `assertCarryFlagNotSet` macro
- `assertDecimalFlagNotSet` macro
- `assertNegativeFlagNotSet` macro
- `assertOverflowFlagNotSet` macro
- `assertZeroFlagNotSet` macro

### Changed
- foreground and background colours of c64unit
- BASIC startup formula due to deprecation in 64tass 1.53

###Removed
- Absolute assertions are no longer used as by default, all expected values should be static


## [0.63] - 2018-08-04
### Added
- Absolute assertions to xa65 cross-assembler (removed after, so not mentioned here anymore)
- Examples in README for xa65 cross-assembler


## [0.62] - 2018-08-03
### Added
- Support for xa65 cross-assembler
- Shell script to build c64unit core on *nix

### Changed
- Due to 64tass 1.53 deprecation, all parameters for .enc directive were put in double quotes


## [0.61] - 2018-07-30
### Fixed
- Include paths in README section for ca65 cross-assembler


## [0.6] - 2018-07-29
### Added
- Support for ca65 cross-assembler
- A note in the README about another repository with c64unit for C

### Changed
- Updated LICENSE


## [0.52] - 2018-07-29
### Added
- Displaying happy end message with the amount of assertions passed


## [0.51] - 2018-07-29
### Added
- PETSCII translation for left and right square brackets for custom messages

### Changed
- Switch character set to lowercase, add reverse display mode for the assert message
- Refactor assertion failed messages to the namespace


## [0.5] - 2018-04-15
### Fixed
- Creating an output directory for new cross-assembler implementation if non existent
- Two negative assertion macros for DASM cross-compiler

### Removed
- Removed untracked file for core.bin


## [0.44] 2018-02-01
### Changed
- Refactored symlinking generator


## [0.43] 2018-01-11
### Fixed
- Examples for asserting data sets in Data Sets section in README for each cross-assembler


## [0.42] 2017-08-31
### Added
- Added LICENSE


## [0.41] 2017-08-22
### Changed
- Install script for *nix to be executable

### Fixed
- Install scripts to clone repository over HTTP


## [0.4] 2017-08-15
### Added
- `assertMemoryEqual` function

### Changed
- Moved `assertFlagSet` function down the line in alphabetical order


## [0.3] 2017-08-09
### Added
- Comments for auto-generated symlink files
- Example to README with test suite memory organization
- Several updates to README for ACME cross-assembler and Features section
- Examples repository reference to all functions description in README

### Changed
- Refactored Symlinker directory reference
- Changed custom message functionality for ACME by adding an ending label

### Fixed
- Bug with core installer generation for Symlinker
- Fixed "if" condition for bash file to lowercase


## [0.22] 2017-07-30
### Added
- Absolute expected value for word assertions for 64tass and DASM cross-assemblers
- Binaries creation for all memory locations in Symlinker

### Changed
- Simplify Kick Assembler implementation for passing immediate and absolute values


## [0.21] 2017-07-29
### Added
- `assertNotEqual` and `assertWordNotEqual` functions

### Changed
- Update installing c64unit vendor dependency scripts


## [0.2] 2017-07-29
### Added
- Introducing support for ACME cross-assembler


## [0.13] 2017-07-27
### Added
- `assertDataSetWordEqual` function
- Data Set examples for word assertions to README
- Examples repository link to README

### Changed
- Unified cross-assemblers directory configuration for Symlinker and CoreInstaller
- Updated Features section in README with details about byte and word assertions
- Updated README for Assertions against address memory


## [0.12] 2017-07-23
### Added
- Introduced `AssertWord` function
- Assert message functionality
- Function to display digit on the screen

### Changed
- Moved assertion failed function to main core file
- Extracting 8-bit value to decimal handler to work with 16-bit values


## [0.11] 2017-07-19
### Added
- Added types to comment blocks for all functions

### Changed
- Updated symbol links for core package


## [0.1] 2017-07-12
### Added
- c64unit framework core and built core packages
- Symlinker build framework
- Support for 64tass cross-assembler
- Support for DASM cross-assembler
- Support for Kick Assembler
- README
- c64unit logo image to README


[0.71]: https://bitbucket.org/Commocore/c64unit/branches/compare/HEAD..v0.7#diff
[0.7]: https://bitbucket.org/Commocore/c64unit/branches/compare/v0.7..v0.65#diff
[0.65]: https://bitbucket.org/Commocore/c64unit/branches/compare/e9b1f343c9bc97cacd29808ce74de11a53c28170..c5ecbe391b9069b4188677c58ca8663546a4d508#diff
[0.64]: https://bitbucket.org/Commocore/c64unit/branches/compare/c5ecbe391b9069b4188677c58ca8663546a4d508..73f7997c9beda5e9e840765b4a9e4b2a4bf64654#diff
[0.63]: https://bitbucket.org/Commocore/c64unit/branches/compare/73f7997c9beda5e9e840765b4a9e4b2a4bf64654..86d643b2b63b3908e65b7f8b029ec818c30b01e8#diff
[0.62]: https://bitbucket.org/Commocore/c64unit/branches/compare/86d643b2b63b3908e65b7f8b029ec818c30b01e8..5a7fe1bdbf4ec53afdb286e03c04306af9cb88da#diff
[0.61]: https://bitbucket.org/Commocore/c64unit/branches/compare/5a7fe1bdbf4ec53afdb286e03c04306af9cb88da..54ae31675f8c019740eaa309219946eb7991edc3#diff
[0.6]: https://bitbucket.org/Commocore/c64unit/branches/compare/54ae31675f8c019740eaa309219946eb7991edc3..92338b0fa3a41a6cb5109ce6c819775df8075c79#diff
[0.52]: https://bitbucket.org/Commocore/c64unit/branches/compare/92338b0fa3a41a6cb5109ce6c819775df8075c79..568cd2e90f69761dab47f636e6fb453928efd001#diff
[0.51]: https://bitbucket.org/Commocore/c64unit/branches/compare/568cd2e90f69761dab47f636e6fb453928efd001..b947bddc352b3684d5ea1574ed289b496e709bd7#diff
[0.5]: https://bitbucket.org/Commocore/c64unit/branches/compare/b947bddc352b3684d5ea1574ed289b496e709bd7..80a0b67115fcb7181b788ce4627054a22eed85ab#diff
[0.44]: https://bitbucket.org/Commocore/c64unit/branches/compare/80a0b67115fcb7181b788ce4627054a22eed85ab..1b11176b8dfbc86a9aafc4f165624379d988d0f2#diff
[0.43]: https://bitbucket.org/Commocore/c64unit/branches/compare/1b11176b8dfbc86a9aafc4f165624379d988d0f2..31ddc075238c53ece9a094d5daada6c99c87f86b#diff
[0.42]: https://bitbucket.org/Commocore/c64unit/branches/compare/31ddc075238c53ece9a094d5daada6c99c87f86b..488be0aeca1f6b2fc9a0089bae1113143f8011c4#diff
[0.41]: https://bitbucket.org/Commocore/c64unit/branches/compare/488be0aeca1f6b2fc9a0089bae1113143f8011c4..d8be3c03fd25e5e8f7a94ddd1ce909c822307fdb#diff
[0.4]: https://bitbucket.org/Commocore/c64unit/branches/compare/d8be3c03fd25e5e8f7a94ddd1ce909c822307fdb..4322ea14208932f5eb8b3442b0e2958e62679807#diff
[0.3]: https://bitbucket.org/Commocore/c64unit/branches/compare/4322ea14208932f5eb8b3442b0e2958e62679807..c8a60fae4ec8cd734bbb8d062d675a0a15ef8915#diff
[0.22]: https://bitbucket.org/Commocore/c64unit/branches/compare/c8a60fae4ec8cd734bbb8d062d675a0a15ef8915..81d7fd2264b6b6d73007d6352771c30beedd7961#diff
[0.21]: https://bitbucket.org/Commocore/c64unit/branches/compare/81d7fd2264b6b6d73007d6352771c30beedd7961..40cf5f4f57da22a3f471022e97253c94c2b9136d#diff
[0.2]: https://bitbucket.org/Commocore/c64unit/branches/compare/40cf5f4f57da22a3f471022e97253c94c2b9136d..7fda34bfb2873a5ee6529e8c1649cf5916e31be2#diff
[0.13]: https://bitbucket.org/Commocore/c64unit/branches/compare/7fda34bfb2873a5ee6529e8c1649cf5916e31be2..51d8e529ce677ce8532bb95497e4180baa33f6cd#diff
[0.12]: https://bitbucket.org/Commocore/c64unit/branches/compare/51d8e529ce677ce8532bb95497e4180baa33f6cd..b78fec49e76c68a0b3cbc1a52d14baf4f97d577f#diff
[0.11]: https://bitbucket.org/Commocore/c64unit/branches/compare/b78fec49e76c68a0b3cbc1a52d14baf4f97d577f..6b20100ed6ee67c81e3adbb1d129e6fa1c15d2e8#diff
[0.1]: https://bitbucket.org/Commocore/c64unit/branches/compare/6b20100ed6ee67c81e3adbb1d129e6fa1c15d2e8..061e8d267e78f43cc823c0fc68de846d64280461#diff
