﻿
; @access public
; @param X - original method lo-address byte
; @param Y - original method hi-address byte
; @return void
c64unit_SetOriginalMethodPointer
	stx c64unit_OriginalMethodOperate+1
	sty c64unit_OriginalMethodOperate+2
rts


; @access public
; @param X - mock method lo-address byte
; @param Y - mock method hi-address byte
; @return void
c64unit_SetMockMethodPointer
	stx c64unit_mockMethodPointerLoByte+1
	sty c64unit_mockMethodPointerHiByte+1
rts


; @access public
; @return void
c64unit_MockMethod

	; Check if method already has been mocked
	lda c64unit_mockMethodTable-5,x
	cmp c64unit_OriginalMethodOperate+1
	bne +
		lda c64unit_mockMethodTable-4,x
		cmp c64unit_OriginalMethodOperate+2
		bne +
			; Method already mocked, exit
			rts
+
	txa
	sec
	sbc #5
	beq +
		tax
		jmp c64unit_MockMethod
+

	; Method is not mocked yet, check mocked methods limit
	lda c64unit_mockMethodPointer
	cmp #5 * MAX_MOCK_METHODS
	bne +
		; Cannot create more mocked methods per test
		lda #size(c64unit_MockMethodFail)
		ldx #<c64unit_MockMethodFail
		ldy #>c64unit_MockMethodFail
		jsr c64unit_PrepareCustomMessage
		jsr c64unit_DisplayCustomMessage
		jsr c64unit_DisplaySummary
		jmp c64unit_ExitWithFail
+
	
	; Preserve first three bytes from original method which will be overwritten by jump to the mock method
	jsr c64unit_SetOperateOnOriginalMethodToLda
	
	ldx c64unit_mockMethodPointer

	; Preserve lo-byte address of original method
	lda c64unit_OriginalMethodOperate+1
	sta c64unit_mockMethodTable,x
	
	; Preserve hi-byte address of original method
	inx
	lda c64unit_OriginalMethodOperate+2
	sta c64unit_mockMethodTable,x
	
	; Preserve first byte of original method
	inx
	ldy #0
	jsr c64unit_OriginalMethodOperate
	sta c64unit_mockMethodTable,x
	
	; Preserve second byte of original method
	inx
	ldy #1
	jsr c64unit_OriginalMethodOperate
	sta c64unit_mockMethodTable,x
	
	; Preserve third byte of original method
	inx
	ldy #2
	jsr c64unit_OriginalMethodOperate
	sta c64unit_mockMethodTable,x

	; Overwrite original method with jump to mock method
	jsr c64unit_SetOperateOnOriginalMethodToSta
	lda #$4c ; jmp mnemonic
	ldy #0
	jsr c64unit_OriginalMethodOperate
	
c64unit_mockMethodPointerLoByte
	lda #0 ; value modified on the fly
	ldy #1
	jsr c64unit_OriginalMethodOperate
	
c64unit_mockMethodPointerHiByte
	lda #0 ; value modified on the fly
	ldy #2
	jsr c64unit_OriginalMethodOperate
	
	; Increment mock method pointer
	clc
	lda c64unit_mockMethodPointer 
	adc #5 ; TODO: Set limit!
	sta c64unit_mockMethodPointer
rts


; @access private
; @return void
c64unit_SetOperateOnOriginalMethodToLda
	lda #$b9 ; lda,y mnemonic
	sta c64unit_OriginalMethodOperate
rts


; @access private
; @return void
c64unit_SetOperateOnOriginalMethodToSta
	lda #$99 ; sta,y mnemonic
	sta c64unit_OriginalMethodOperate
rts


; @access private
; @param Y - offset
; @param A - if method operates in sta mode
; @return A - if method operates in lda mode
c64unit_OriginalMethodOperate
	lda $1234,y ; mnemonic is changed on the fly between lda,y and sta,y
rts


; @access public
; @param X - original method lo-address byte
; @param Y - original method hi-address byte
; @return void
c64unit_UnmockMethod
	stx c64unit_UnmockMethodLoByte+1
	sty c64unit_UnmockMethodHiByte+1
	
	ldx c64unit_mockMethodPointer
	bne +
		rts
+

c64unit_UnmockMethodSearchLoop
	lda c64unit_mockMethodTable-5,x
c64unit_UnmockMethodLoByte
	cmp #0
	bne +
		lda c64unit_mockMethodTable-4,x
c64unit_UnmockMethodHiByte
		cmp #0
		bne +
			jmp c64unit_UnmockMethodByPointer
+
	txa
	sec
	sbc #5
	bne +
		rts
+
	tax
	jmp c64unit_UnmockMethodSearchLoop


; @access public
; @return void
c64unit_UnmockAllMethods
	lda c64unit_mockMethodPointer
	bne +
		rts
+

c64unit_UnmockAllMethodsLoop
	tax
	jsr c64unit_UnmockMethodByPointer
	lda c64unit_mockMethodPointer
	sec
	sbc #5
	sta c64unit_mockMethodPointer
	bne c64unit_UnmockAllMethodsLoop
rts


; @access private
; @param X - mock method pointer
; @return void
c64unit_UnmockMethodByPointer
	; Exit if there is no method mock for this pointer
	lda c64unit_mockMethodTable-5,x
	bne +
		lda c64unit_mockMethodTable-4,x
		bne +
			rts
+

	; Get address of original method
	lda c64unit_mockMethodTable-5,x
	sta c64unit_OriginalMethodOperate+1
	
	lda c64unit_mockMethodTable-4,x
	sta c64unit_OriginalMethodOperate+2
	
	jsr c64unit_SetOperateOnOriginalMethodToSta
	
	; Restore first byte
	lda c64unit_mockMethodTable-3,x
	ldy #0
	jsr c64unit_OriginalMethodOperate
	
	; Restore second byte
	lda c64unit_mockMethodTable-2,x
	iny
	jsr c64unit_OriginalMethodOperate
	
	; Restore third byte
	lda c64unit_mockMethodTable-1,x
	iny
	jsr c64unit_OriginalMethodOperate
	
	; Remove mock method register from mockMethodTable
	lda #0
	sta c64unit_mockMethodTable-5,x
	sta c64unit_mockMethodTable-4,x
	sta c64unit_mockMethodTable-3,x
	sta c64unit_mockMethodTable-2,x
	sta c64unit_mockMethodTable-1,x
rts


c64unit_mockMethodTable
	.for _methods=1, _methods<=MAX_MOCK_METHODS, _methods=_methods+1
		.word 0 ; original method 16-bit address
		.byte 0 ; first byte of original method
		.byte 0 ; second byte of original method
		.byte 0 ; third byte of original method
	.next


c64unit_mockMethodPointer
	.byte 0
