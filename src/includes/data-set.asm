
; @access public
; @param A - length of Data Set
; @return void
c64unit_PrepareDataSetLength
	sta c64unit_DataSetLength + 1
	lda #0
	sta c64unit_dataSetIterator
rts


; @access public
; @param X - lo-byte address of Data Set
; @param Y - hi-byte address of Data Set
; @return void
c64unit_LoadDataSet .block
	stx inputPointer + 1
	sty inputPointer + 2
	
	ldy c64unit_dataSetIterator
inputPointer
	lda $1234,y
outputPointer
	sta $1234
rts

.bend


; @access public
; @param X - lo-byte address of Data Set
; @param Y - hi-byte address of Data Set
; @return A - value
c64unit_LoadDataSetToA .block
	stx inputPointer + 1
	sty inputPointer + 2
	
	ldy c64unit_dataSetIterator
inputPointer
	lda $1234,y
rts

.bend


; Gets word from given Data Set memory location and puts into output memory location
; which needs to be set beforehand in c64unit_PrepareDataSetWordOutput method
; 
; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @return void
c64unit_LoadDataSetWord .block
	stx inputPointer + 1
	sty inputPointer + 2
	
	lda c64unit_dataSetIterator
	asl
	tay
	ldx #0
-
inputPointer
	lda $1234,y
outputPointer
	sta $1234,x
	iny
	inx
	cpx #2
	bne -
rts

.bend


; Gets word from given Data Set memory location and puts into output memory location for lo and hi byte
; which needs to be set beforehand in c64unit_PrepareDataSetWordToLoHiOutput method
; 
; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @return void
c64unit_LoadDataSetWordToLoHi .block
	stx inputPointerForLoPart + 1
	sty inputPointerForLoPart + 2
	stx inputPointerForHiPart + 1
	sty inputPointerForHiPart + 2
	
	lda c64unit_dataSetIterator
	asl
	tay
inputPointerForLoPart
	lda $1234,y
outputPointerForLoPart
	sta $1234
	
	iny
inputPointerForHiPart
	lda $1234,y
outputPointerForHiPart
	sta $1234
rts

.bend


; @access public
; @return true for zero flag if completed
c64unit_IsDataSetCompleted
	inc c64unit_dataSetIterator
	lda c64unit_dataSetIterator
c64unit_DataSetLength
	cmp #0
rts


; Prepares memory location for byte which will be taken from Data Set
; 
; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @return void
c64unit_PrepareDataSetOutput
	stx c64unit_LoadDataSet.outputPointer + 1
	sty c64unit_LoadDataSet.outputPointer + 2
rts


; Prepares memory location for word which will be taken from Data Set
; 
; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @return void
c64unit_PrepareDataSetWordOutput
	stx c64unit_LoadDataSetWord.outputPointer + 1
	sty c64unit_LoadDataSetWord.outputPointer + 2
rts


; Prepares memory location for word which will be taken from Data Set
; It's used within c64unit_GetDataSetWordToLoHi which allocates the hi part destination
; 
; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @return void
c64unit_PrepareDataSetWordHiOutput
	stx c64unit_LoadDataSetWordToLoHi.outputPointerForHiPart + 1
	sty c64unit_LoadDataSetWordToLoHi.outputPointerForHiPart + 2
rts


; Prepares memory location for word which will be taken from Data Set
; It's used within c64unit_GetDataSetWordToLoHi which allocates the lo part destination
; 
; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @return void
c64unit_PrepareDataSetWordLoOutput
	stx c64unit_LoadDataSetWordToLoHi.outputPointerForLoPart + 1
	sty c64unit_LoadDataSetWordToLoHi.outputPointerForLoPart + 2
rts
