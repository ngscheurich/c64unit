
; @access public
; @uses c64unit_numberLo
; @uses c64unit_numberHi
; @uses c64unit_newNumberLo
; @uses c64unit_newNumberHi
; @uses c64unit_number_1
; @uses c64unit_number_10
; @uses c64unit_number_100
; @uses c64unit_number_1000
; @uses c64unit_number_10000
; @return void
c64unit_Extract16bitValueToDec
	lda #0
	sta c64unit_number10000
	sta c64unit_number1000
	sta c64unit_number100
	sta c64unit_number10
	sta c64unit_number1

	; tens of thousands
	sec
-
	lda c64unit_numberLo
	sbc #<10000
	sta c64unit_newNumberLo
	lda c64unit_numberHi
	sbc #>10000
	sta c64unit_newNumberHi
	
	bcc +
		lda c64unit_newNumberLo
		sta c64unit_numberLo
		lda c64unit_newNumberHi
		sta c64unit_numberHi
		inc c64unit_number10000
		jmp -
+
	
	; thousands
	sec
-
	lda c64unit_numberLo
	sbc #<1000
	sta c64unit_newNumberLo
	lda c64unit_numberHi
	sbc #>1000
	sta c64unit_newNumberHi
	
	bcc +
		lda c64unit_newNumberLo
		sta c64unit_numberLo
		lda c64unit_newNumberHi
		sta c64unit_numberHi
		inc c64unit_number1000
		jmp -
+

	; hundrers
	sec
-
	lda c64unit_numberLo
	sbc #<100
	sta c64unit_newNumberLo
	lda c64unit_numberHi
	sbc #>100
	sta c64unit_newNumberHi
	
	bcc +
		lda c64unit_newNumberLo
		sta c64unit_numberLo
		lda c64unit_newNumberHi
		sta c64unit_numberHi
		inc c64unit_number100
		jmp -
+

	; tens
	sec
-
	lda c64unit_numberLo
	sbc #<10
	sta c64unit_newNumberLo
	bcc +
		lda c64unit_newNumberLo
		sta c64unit_numberLo
		inc c64unit_number10
		jmp -
+

	; ones
	clc
	lda c64unit_numberLo
	sta c64unit_number1
rts


; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @uses c64unit_actual
; @return byte value
c64unit_SetActualAbsoluteByte
	lda #0
	sta c64unit_actual+1
	
	stx c64unit_SetActualAbsoluteBytePointer+1
	sty c64unit_SetActualAbsoluteBytePointer+2
c64unit_SetActualAbsoluteBytePointer
	lda $1234
	sta c64unit_actual
rts


; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @uses c64unit_actual
; @return void
c64unit_SetActualAbsoluteWord
	stx c64unit_SetActualAbsoluteWordPointer+1
	sty c64unit_SetActualAbsoluteWordPointer+2
	ldy #1
c64unit_SetActualAbsoluteWordPointer
-
	lda $1234,y
	sta c64unit_actual,y
	dey
	bpl -
rts


; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @uses c64unit_expected
; @return byte value
c64unit_SetExpectedAbsoluteByte
	lda #0
	sta c64unit_expected+1
	
	stx c64unit_SetExpectedAbsoluteBytePointer+1
	sty c64unit_SetExpectedAbsoluteBytePointer+2
c64unit_SetExpectedAbsoluteBytePointer
	lda $1234
	sta c64unit_expected
rts


; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @uses c64unit_expected
; @return void
c64unit_SetExpectedAbsoluteWord
	stx c64unit_SetExpectedAbsoluteWordPointer+1
	sty c64unit_SetExpectedAbsoluteWordPointer+2
	ldy #1
	lda c64unit_dataSetIterator
	asl
	tax
	inx
c64unit_SetExpectedAbsoluteWordPointer
-
	lda $1234,x
	sta c64unit_expected,y
	dex
	dey
	bpl -
rts


; @access public
; @param X - lo-byte address
; @param Y - hi-byte address
; @uses c64unit_expected
; @return void
c64unit_SetExpectedImmediateWord
	stx c64unit_expected
	sty c64unit_expected+1
rts
