
.weak

.enc "screen"

MAX_MOCK_METHODS = 30

c64unit_AssertionFailDetails .text "data #@. expected: @, actual: @."

c64unit_MockMethodFail .text "cannot mock more methods per test"

.enc "none"

.endweak
