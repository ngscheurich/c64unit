
; @access public
; @return void
c64unit_InitScreen
	; Clear screen using KERNAL CHROUT
	lda #147
	jsr $ffd2
	
	; Set foreground and background colour
	lda #12
	sta $d020
	lda #11
	sta $d021
	
	; Switch character set to lowercase
	lda #23
	sta $d018
	
	jsr c64unit_InitProgressIndicator
rts


; @access private
; @param A - value
; @param Y - offset
; @continue c64unit_PutCharacter
c64unit_PutDigit
	adc #48


; @access private
; @param A - value
; @param Y - offset
; @return void
c64unit_PutCharacter
	sta $1234,y
rts


; @access public
; @var byte c64unit_number100 [needs to be set beforehand]
; @var byte c64unit_number10 [needs to be set beforehand]
; @var byte c64unit_number1 [needs to be set beforehand]
; @var Y - position on the screen [needs to be set beforehand]
; @return void
c64unit_DisplayNumber
	clc
	lda c64unit_number10000
	bne +
		lda c64unit_number1000
		bne ++
			lda c64unit_number100
			bne +++
				lda c64unit_number10
				bne ++++
					jmp +++++
+
	jsr c64unit_PutDigit
	iny
	lda c64unit_number1000
+
	jsr c64unit_PutDigit
	iny
	lda c64unit_number100
+
	jsr c64unit_PutDigit
	iny
	lda c64unit_number10
+
	jsr c64unit_PutDigit
	iny
+
	lda c64unit_number1
	jsr c64unit_PutDigit
	iny
rts
