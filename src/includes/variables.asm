
.weak

c64unit_exitToBasicMode .byte 1 ; exit to Basic when finished / assertion failed or do infinity loop?
c64unit_screenLocation .word $0400
c64unit_dataSetIterator .word 0
c64unit_expected .word 0
c64unit_actual .word 0
c64unit_number1 .byte 0
c64unit_number10 .byte 0
c64unit_number100 .byte 0
c64unit_number1000 .byte 0
c64unit_number10000 .byte 0
c64unit_numberLo .byte 0
c64unit_numberHi .byte 0
c64unit_newNumberLo .byte 0
c64unit_newNumberHi .byte 0

c64unit_number = c64unit_number1

.endweak
