﻿
; @access public
; @return void
c64unit_CleanMessageField
	ldy #79
	lda #32
-
	jsr c64unit_PutCharacter
	dey
	bpl -
rts


; @access public
; @return void
c64unit_DisplayAssertionFail
	cld
	jsr c64unit_CleanMessageField
	
	lda c64unit_CustomMessageLength+1
	beq +
		jsr c64unit_DisplayCustomMessage
+
	jsr c64unit_DisplaySummary
rts


; @access private
; @return void
c64unit_DisplayCustomMessage
	ldx #0
	ldy #0
-
c64unit_DisplayCustomMessagePointer
	lda $1234,x
	jsr c64unit_PutCharacter
	inx
	iny
c64unit_CustomMessageLength
	cpx #0
	bne -
rts


; @access private
; @return void
c64unit_DisplayAssertMessage
	ldx #0
	ldy #40
	clc
-
c64unit_DisplayAssertMessagePointer
	lda $1234,x
	adc #128 ; reverse foreground and background
	jsr c64unit_PutCharacter
	inx
	iny
c64unit_AssertMessageLength
	cpx #0
	bne -
rts


; @access public
; @return void
c64unit_DisplaySummary
	lda #0
	sta c64unit_DisplaySummaryValue+1
	
	jsr c64unit_DisplayAssertMessage
	
	ldy #80
	ldx #0
	
	; If it's not a Data Set, do not display set counter on the beginning of the message
	lda c64unit_DataSetLength+1
	bne +
		ldx #9
		inc c64unit_DisplaySummaryValue+1
+

-
	lda c64unit_AssertionFailDetails,x
	bne ++++
		inc c64unit_DisplaySummaryValue+1
c64unit_DisplaySummaryValue
		lda #0
		cmp #1
		bne +
			lda c64unit_dataSetIterator
			sta c64unit_numberLo
			lda c64unit_dataSetIterator+1
			sta c64unit_numberHi
			jmp +++
+
		cmp #2
		bne +
			lda c64unit_expected
			sta c64unit_numberLo
			lda c64unit_expected+1
			sta c64unit_numberHi
			jmp ++
+
		lda c64unit_actual
		sta c64unit_numberLo
		lda c64unit_actual+1
		sta c64unit_numberHi
+
		jsr c64unit_Extract16bitValueToDec
		jsr c64unit_DisplayNumber
		jmp ++
+
	jsr c64unit_PutCharacter
	iny
+
	inx
	cpx #size(c64unit_AssertionFailDetails)
	bne -
rts


; @access public
; @param X - message lo-address
; @param Y - message hi-address
; @param A - length of a message
; @return void
c64unit_PrepareCustomMessage
	stx c64unit_DisplayCustomMessagePointer+1
	sty c64unit_DisplayCustomMessagePointer+2

	; Check size of a message
	cmp #40
	bcc +
		lda #40
+
	sta c64unit_CustomMessageLength+1
rts


; @access private
; @param X - message lo-address
; @param Y - message hi-address
; @param A - length of a message
; @return void
c64unit_PrepareAssertMessage
	stx c64unit_DisplayAssertMessagePointer+1
	sty c64unit_DisplayAssertMessagePointer+2
	
	; Check size of a message
	cmp #40
	bcc +
		lda #40
+
	sta c64unit_AssertMessageLength+1
rts


; Convert characters (needed for all versions of DASM without SCRU, and xa65)
;
; @access public
; @param X - message lo-address
; @param Y - message hi-address
; @param A - length of a message
; @return void
c64unit_PrepareCustomMessageWithConversion
	jsr c64unit_PrepareCustomMessage
	stx c64unit_ConvertCustomMessageLoad+1
	sty c64unit_ConvertCustomMessageLoad+2
	stx c64unit_ConvertCustomMessageSave+1
	sty c64unit_ConvertCustomMessageSave+2
	tay
	dey
c64unit_ConvertCustomMessageLoad
	lda $1234,y
	cmp #$20 ; space character
	beq c64unit_ConvertCustomMessageSave ; do not convert
	cmp #$5b ; numbers and special characters
	bcc c64unit_ConvertCustomMessageSave
	bne +
		lda #27 ; left square bracket
		jmp c64unit_ConvertCustomMessageSave
+
	cmp #$5d
	bne +
		lda #29 ; right square bracket
		jmp c64unit_ConvertCustomMessageSave
+
	sec
	sbc #$60
c64unit_ConvertCustomMessageSave
	sta $1234,y
	dey
	bpl c64unit_ConvertCustomMessageLoad
rts


; @access private
; @return void
c64unit_DisplayHappyEndMessage
	; Display the first line
	clc
	lda c64unit_screenLocation
	adc #<880
	sta c64unit_PutCharacter+1
	lda c64unit_screenLocation+1
	adc #>880
	sta c64unit_PutCharacter+2
	
	ldx #0
	ldy #40
	clc
-
	lda c64unit_HappyEndMessage.allTestsPassed,x
	adc #128 ; reverse foreground and background
	jsr c64unit_PutCharacter
	iny
	inx
	cpx #size(c64unit_HappyEndMessage.allTestsPassed)
	bne -
	
	; Display the second line
	clc
	lda c64unit_screenLocation
	adc #<920
	sta c64unit_PutCharacter+1
	lda c64unit_screenLocation+1
	adc #>920
	sta c64unit_PutCharacter+2
	
	ldx #0
	ldy #40
	clc
-
	lda c64unit_HappyEndMessage.numberOfAssertions,x
	jsr c64unit_PutCharacter
	iny
	inx
	cpx #size(c64unit_HappyEndMessage.numberOfAssertions)
	bne -
	
	; Get number of assertions executed from progress indicator
	sec
	lda c64unit_ProgressIndicatorPointer+1
	sbc c64unit_screenLocation
	sta c64unit_numberLo
	lda c64unit_ProgressIndicatorPointer+2
	sbc c64unit_screenLocation+1
	sta c64unit_numberHi

	; Align number of assertions which were counted from 0
	clc
	lda c64unit_numberLo
	adc #1
	sta c64unit_numberLo
	lda c64unit_numberHi
	adc #0
	sta c64unit_numberHi
	
	jsr c64unit_Extract16bitValueToDec
	jsr c64unit_DisplayNumber
rts


.enc "screen"

c64unit_HappyEndMessage .proc
allTestsPassed .text "all tests passed."
numberOfAssertions .text "number of assertions: "
.pend

.enc "none"
