
; @access public
; @param A - when tests finished / failed: 0 - infinity loop, 1 - back to Basic
; @return void
c64unit_InitExitToBasicMode
	and #%00000001
	sta c64unit_exitToBasicMode
rts


; @access public
; @param X - screen location lo-byte address
; @param Y - screen location hi-byte address
; @return void
c64unit_InitScreenLocation
	stx c64unit_screenLocation
	sty c64unit_screenLocation+1
	
	clc
	lda c64unit_screenLocation
	adc #<880
	sta c64unit_PutCharacter+1
	lda c64unit_screenLocation+1
	adc #>880
	sta c64unit_PutCharacter+2
rts


; @access public
; @return void
c64unit_InitTest
	lda #0
	sta c64unit_expected
	sta c64unit_expected+1
	sta c64unit_actual
	sta c64unit_actual+1
	sta c64unit_CustomMessageLength+1
	sta c64unit_DataSetLength+1
	jsr c64unit_ResetDataSetIterator
	jsr c64unit_UnmockAllMethods
rts


; @access public
; @return zero flag
c64unit_IsExitToBasicMode
	lda c64unit_exitToBasicMode
rts


; Reset iterator in test suite for each Data Set to distinct for what data an assertion exactly fails
;
; @access public
; @return void
c64unit_ResetDataSetIterator
	lda #0
	sta c64unit_dataSetIterator
rts


; Global function to exit from test suite with failed assertion
; 
; @access private
; @noreturn
c64unit_AssertionFailed
	jsr c64unit_DisplayAssertionFail
	jmp c64unit_ExitWithFail


; Assert failed, halt tests and show red border
;
; @access public
; @noreturn
c64unit_ExitWithFail
	; Change border colour to red
	lda #2
	sta $d020
	
	jsr c64unit_IsExitToBasicMode
	bne +
		jmp *
+
	jsr c64unit_RestoreBasicValues
	jmp c64unit_ExitToBasic


; @access public
; @noreturn
c64unit_ExitWithSuccess
	; Change border colour to green
	lda #5
	sta $d020
	
	jsr c64unit_DisplayHappyEndMessage
	jsr c64unit_IsExitToBasicMode
	bne +
		jmp *
+
	jsr c64unit_RestoreBasicValues
	jmp c64unit_ExitToBasic


; @access private
; @return void
c64unit_ExitToBasic
	; Return to BASIC by resetting stack pointer
	ldx #$f6
	txs
rts


; Restore BASIC settings, otherwise it can crash if some parts of zero page has been used
;
; @access private
; @return void
c64unit_RestoreBasicValues
	; Default I/O device to keyboard input
	lda #0
	sta $13

	; Default pointer to next expression in string stack, otherwise e.g. it can crash at $B4E0 for #$1
	lda #$19
	sta $16
	
	jsr c64unit_SetCursorRowOnExitToBasic
rts


; Set cursor below indicator progress when exiting to Basic
;
; @access private
; @return void
c64unit_SetCursorRowOnExitToBasic
	lda #0
	sta $d6
rts


; Include methods
.include "includes/constants.asm"
.include "includes/variables.asm"
.include "includes/handlers.asm"
.include "includes/assertions.asm"
.include "includes/data-set.asm"
.include "includes/mock-method.asm"
.include "includes/display.asm"
.include "includes/messages.asm"
.include "includes/progress-indicator.asm"
