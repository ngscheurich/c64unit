
; Add srccode macro to display screen codes properly for custom messages
.macpack cbm


; @param word expected
; @param word actual
; @param string customMessage = ""
.macro assertAbsoluteWordEqual expected, actual, customMessage
	ldx #<expected
	ldy #>expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertWordEqual
.endmacro


; @param word expected
; @param word actual
; @param string customMessage = ""
.macro assertAbsoluteWordNotEqual expected, actual, customMessage
	ldx #<expected
	ldy #>expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertWordNotEqual
.endmacro


; @param string customMessage = ""
.macro assertCarryFlagNotSet customMessage
	jsr c64unit_StoreNotCarryFlagStatus
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param string customMessage = ""
.macro assertCarryFlagSet customMessage
	jsr c64unit_StoreCarryFlagStatus
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param word expectedDataSet
; @param byte actual
; @param string customMessage = ""
.macro assertDataSetEqual expectedDataSet, actual, customMessage
	lda actual
	sta c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_AssertDataSetEqual
.endmacro


; @param word expectedDataSet
; @param string customMessage = ""
.macro assertDataSetEqualToA expectedDataSet, customMessage
	sta c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_AssertDataSetEqual
.endmacro


; @param word expectedDataSet
; @param string customMessage = ""
.macro assertDataSetEqualToX expectedDataSet, customMessage
	stx c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_AssertDataSetEqual
.endmacro


; @param word expectedDataSet
; @param string customMessage = ""
.macro assertDataSetEqualToY expectedDataSet, customMessage
	sty c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_AssertDataSetEqual
.endmacro


; @param word expectedDataSet
; @param word actual
; @param string customMessage = ""
.macro assertDataSetWordEqual expectedDataSet, actual, customMessage
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertWordEqual
.endmacro


; @param string customMessage = ""
.macro assertDecimalFlagNotSet customMessage
	jsr c64unit_StoreNotDecimalFlagStatus
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param string customMessage = ""
.macro assertDecimalFlagSet customMessage
	jsr c64unit_StoreDecimalFlagStatus
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param byte expected
; @param byte actual
; @param string customMessage = ""
.macro assertEqual expected, actual, customMessage
	lda expected
	sta c64unit_expected
	lda actual
	sta c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertEqual
.endmacro


; @param byte expected
; @param string customMessage = ""
.macro assertEqualToA expected, customMessage
	sta c64unit_actual
	lda expected
	sta c64unit_expected
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertEqual
.endmacro


; @param byte expected
; @param string customMessage = ""
.macro assertEqualToX expected, customMessage
	stx c64unit_actual
	lda expected
	sta c64unit_expected
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertEqual
.endmacro


; @param byte expected
; @param string customMessage = ""
.macro assertEqualToY expected, customMessage
	sty c64unit_actual
	lda expected
	sta c64unit_expected
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertEqual
.endmacro


; @param byte expected
; @param byte actual
; @param string customMessage = ""
.macro assertGreater expected, actual, customMessage
	lda expected
	sta c64unit_expected
	lda actual
	sta c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertGreater
.endmacro


; @param byte expected
; @param byte actual
; @param string customMessage = ""
.macro assertGreaterOrEqual expected, actual, customMessage
	lda expected
	sta c64unit_expected
	lda actual
	sta c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertGreaterOrEqual
.endmacro


; @param byte expected
; @param byte actual
; @param string customMessage = ""
.macro assertLess expected, actual, customMessage
	lda expected
	sta c64unit_expected
	lda actual
	sta c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertLess
.endmacro


; @param word expected
; @param word actual
; @param word length
; @param string customMessage = ""
.macro assertMemoryEqual expected, actual, length, customMessage
	lda #<expected
	sta c64unit_ExpectedMemoryLocationPointer+1
	lda #>expected
	sta c64unit_ExpectedMemoryLocationPointer+2
	
	lda #<actual
	sta c64unit_ActualMemoryLocationPointer+1
	lda #>actual
	sta c64unit_ActualMemoryLocationPointer+2
	
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	
	ldx #<length
	ldy #>length
	jsr c64unit_AssertMemoryEqual
.endmacro


; @param string customMessage = ""
.macro assertNegativeFlagSignedNotSet customMessage
	jsr c64unit_StoreNotNegativeFlagStatusSigned
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param string customMessage = ""
.macro assertNegativeFlagSignedSet customMessage
	jsr c64unit_StoreNegativeFlagStatusSigned
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param string customMessage = ""
.macro assertNegativeFlagUnsignedNotSet customMessage
	jsr c64unit_StoreNotNegativeFlagStatusUnsigned
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param string customMessage = ""
.macro assertNegativeFlagUnsignedSet customMessage
	jsr c64unit_StoreNegativeFlagStatusUnsigned
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param byte expected
; @param byte actual
; @param string customMessage = ""
.macro assertNotEqual expected, actual, customMessage
	lda expected
	sta c64unit_expected
	lda actual
	sta c64unit_actual
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertNotEqual
.endmacro


; @param byte expected
; @param string customMessage = ""
.macro assertNotEqualToA expected, customMessage
	sta c64unit_actual
	lda expected
	sta c64unit_expected
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertNotEqual
.endmacro


; @param byte expected
; @param string customMessage = ""
.macro assertNotEqualToX expected, customMessage
	stx c64unit_actual
	lda expected
	sta c64unit_expected
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertNotEqual
.endmacro


; @param byte expected
; @param string customMessage = ""
.macro assertNotEqualToY expected, customMessage
	sty c64unit_actual
	lda expected
	sta c64unit_expected
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertNotEqual
.endmacro


; @param string customMessage = ""
.macro assertOverflowFlagNotSet customMessage
	jsr c64unit_StoreNotOverflowFlagStatus
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param string customMessage = ""
.macro assertOverflowFlagSet customMessage
	jsr c64unit_StoreOverflowFlagStatus
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param word expected
; @param word actual
; @param string customMessage = ""
.macro assertWordEqual expected, actual, customMessage
	ldx #<expected
	ldy #>expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertWordEqual
.endmacro


; @param word expected
; @param word actual
; @param string customMessage = ""
.macro assertWordNotEqual expected, actual, customMessage
	ldx #<expected
	ldy #>expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertWordNotEqual
.endmacro


; @param string customMessage = ""
.macro assertZeroFlagNotSet customMessage
	jsr c64unit_StoreNotZeroFlagStatus
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param string customMessage = ""
.macro assertZeroFlagSet customMessage
	jsr c64unit_StoreZeroFlagStatus
	.ifnblank customMessage
		prepareCustomMessage customMessage
	.endif
	jsr c64unit_AssertFlagSet
.endmacro


; @param byte exitToBasicMode = 1
; @param word screenLocation = ""
.macro c64unit exitToBasicMode, screenLocation
	.local nextBasicLine
	.segment "LOADADDR"
	.addr $0801
	.code
	.word nextBasicLine
	.word 10
	.byte $9e
	.byte "2061"
	.byte 0

nextBasicLine:
	.byte 0, 0
	
	.ifnblank exitToBasicMode
		lda #exitToBasicMode
	.else
		lda #1
	.endif
	jsr c64unit_InitExitToBasicMode
	
	.ifnblank screenLocation
		ldx #<screenLocation
		ldy #>screenLocation
	.else
		ldx #<$0400
		ldy #>$0400
	.endif
	jsr c64unit_InitScreenLocation

	jsr c64unit_InitScreen
.endmacro


.macro c64unitExit
	jmp c64unit_ExitWithSuccess
.endmacro


; @param word testMethod
.macro examineTest testMethod
	jsr c64unit_InitTest
	jsr testMethod
.endmacro


; @param word dataSet
; @param word output
.macro loadDataSet dataSet, output
	ldx #<output
	ldy #>output
	jsr c64unit_PrepareDataSetOutput
	ldx #<dataSet
	ldy #>dataSet
	jsr c64unit_LoadDataSet
.endmacro


; @param word dataSet
.macro loadDataSetToA dataSet
	pushXYOnStack
	ldx #<dataSet
	ldy #>dataSet
	jsr c64unit_LoadDataSetToA
	sta c64unit_number
	pullXYFromStack
	lda c64unit_number
.endmacro


; @param word dataSet
; @param word output
.macro loadDataSetWord dataSet, output
	ldx #<output
	ldy #>output
	jsr c64unit_PrepareDataSetWordOutput
	ldx #<dataSet
	ldy #>dataSet
	jsr c64unit_LoadDataSetWord
.endmacro


; @param word dataSet
; @param word outputLo
; @param word outputHi
.macro loadDataSetWordToLoHi dataSet, outputLo, outputHi
	ldx #<outputLo
	ldy #>outputLo
	jsr c64unit_PrepareDataSetWordLoOutput
	ldx #<outputHi
	ldy #>outputHi
	jsr c64unit_PrepareDataSetWordHiOutput
	ldx #<dataSet
	ldy #>dataSet
	jsr c64unit_LoadDataSetWordToLoHi
.endmacro


.macro initTest
	jsr c64unit_InitTest
.endmacro


; @return carry flag
.macro isDataSetCompleted
	jsr c64unit_IsDataSetCompleted
.endmacro


; @param word originalMethod
; @param word mockMethod
.macro mockMethod originalMethod, mockMethod
	ldx #<originalMethod
	ldy #>originalMethod
	jsr c64unit_SetOriginalMethodPointer
	
	ldx #<mockMethod
	ldy #>mockMethod
	jsr c64unit_SetMockMethodPointer
	
	jsr c64unit_MockMethod
.endmacro


; @param string customMessage
.macro prepareCustomMessage customMessage
	.local text
	.local textEnd
	; Trick to create text label dynamically
	jmp textEnd
text:
	scrcode customMessage ; use srccode macro to display screen codes properly
	
textEnd:
	; Calculate length of a message
	sec
	lda #<textEnd
	sbc #<text
	
	; Convert message characters
	ldx #<text
	ldy #>text
	jsr c64unit_PrepareCustomMessage
.endmacro


; @param byte length
.macro prepareDataSetLength length
	lda #length
	jsr c64unit_PrepareDataSetLength
.endmacro


.macro pushXYOnStack
	txa
	pha
	tya
	pha
.endmacro


.macro pullXYFromStack
	pla
	tay
	pla
	tax
.endmacro


; @param word originalMethod
.macro unmockMethod originalMethod
	ldx #<originalMethod
	ldy #>originalMethod
	jsr c64unit_UnmockMethod
.endmacro



