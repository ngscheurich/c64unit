
; @param word expected
; @param word actual
; @param string customMessage
#define assertAbsoluteWordEqual(expected,actual,customMessage) .(: \
	ldx #<expected : \
	ldy #>expected : \
	jsr c64unit_SetExpectedAbsoluteWord : \
	: \
	ldx #<actual : \
	ldy #>actual : \
	jsr c64unit_SetActualAbsoluteWord : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertWordEqual : \
}


; @param word expected
; @param word actual
; @param string customMessage
#define assertAbsoluteWordNotEqual(expected,actual,customMessage) .(: \
	ldx #<expected : \
	ldy #>expected : \
	jsr c64unit_SetExpectedAbsoluteWord : \
	: \
	ldx #<actual : \
	ldy #>actual : \
	jsr c64unit_SetActualAbsoluteWord : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertWordNotEqual : \
}


; @param string customMessage
#define assertCarryFlagNotSet(customMessage) .(: \
	jsr c64unit_StoreNotCarryFlagStatus : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param string customMessage
#define assertCarryFlagSet(customMessage) .(: \
	jsr c64unit_StoreCarryFlagStatus : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param word expectedDataSet
; @param byte actual
; @param string customMessage
#define assertDataSetEqual(expectedDataSet,actual,customMessage) .(: \
	lda actual : \
	sta c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	ldx #<expectedDataSet : \
	ldy #>expectedDataSet : \
	jsr c64unit_AssertDataSetEqual : \
.)


; @param word expectedDataSet
; @param string customMessage
#define assertDataSetEqualToA(expectedDataSet,customMessage) .(: \
	sta c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	ldx #<expectedDataSet : \
	ldy #>expectedDataSet : \
	jsr c64unit_AssertDataSetEqual : \
.)


; @param word expectedDataSet
; @param string customMessage
#define assertDataSetEqualToX(expectedDataSet,customMessage) .(: \
	stx c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	ldx #<expectedDataSet : \
	ldy #>expectedDataSet : \
	jsr c64unit_AssertDataSetEqual : \
.)


; @param word expectedDataSet
; @param string customMessage
#define assertDataSetEqualToY(expectedDataSet,customMessage) .(: \
	sty c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	ldx #<expectedDataSet : \
	ldy #>expectedDataSet : \
	jsr c64unit_AssertDataSetEqual : \
.)


; @param word expectedDataSet
; @param word actual
; @param string customMessage
#define assertDataSetWordEqual(expectedDataSet,actual,customMessage) .(: \
	ldx #<expectedDataSet : \
	ldy #>expectedDataSet : \
	jsr c64unit_SetExpectedAbsoluteWord : \
	: \
	ldx #<actual : \
	ldy #>actual : \
	jsr c64unit_SetActualAbsoluteWord : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertWordEqual : \
.)


; @param string customMessage
#define assertDecimalFlagNotSet(customMessage) .(: \
	jsr c64unit_StoreNotDecimalFlagStatus : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param string customMessage
#define assertDecimalFlagSet(customMessage) .(: \
	jsr c64unit_StoreDecimalFlagStatus : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param byte expected
; @param byte actual
; @param string customMessage
#define assertEqual(expected,actual,customMessage) .(: \
	lda #expected : \
	sta c64unit_expected : \
	lda actual : \
	sta c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertEqual : \
.)


; @param byte expected
; @param string customMessage
#define assertEqualToA(expected,customMessage) .(: \
	sta c64unit_actual : \
	lda #expected : \
	sta c64unit_expected : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertEqual : \
.)


; @param byte expected
; @param string customMessage
#define assertEqualToX(expected,customMessage) .(: \
	stx c64unit_actual : \
	lda #expected : \
	sta c64unit_expected : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertEqual : \
.)


; @param byte expected
; @param string customMessage
#define assertEqualToY(expected,customMessage) .(: \
	sty c64unit_actual : \
	lda #expected : \
	sta c64unit_expected : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertEqual : \
.)


; @param byte expected
; @param byte actual
; @param string customMessage
#define assertGreater(expected,actual,customMessage) .(: \
	lda #expected : \
	sta c64unit_expected : \
	lda actual : \
	sta c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertGreater :\
.)


; @param byte expected
; @param byte actual
; @param string customMessage
#define assertGreaterOrEqual(expected,actual,customMessage) .(: \
	lda #expected : \
	sta c64unit_expected : \
	lda actual : \
	sta c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertGreaterOrEqual : \
.)


; @param byte expected
; @param byte actual
; @param string customMessage
#define assertLess(expected,actual,customMessage) .(: \
	lda #expected : \
	sta c64unit_expected : \
	lda actual : \
	sta c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertLess : \
.)


; @param word expected
; @param word actual
; @param word length
; @param string customMessage
#define assertMemoryEqual(expected,actual,length,customMessage) .(: \
	lda #<expected : \
	sta c64unit_ExpectedMemoryLocationPointer+1 : \
	lda #>expected : \
	sta c64unit_ExpectedMemoryLocationPointer+2 : \
	: \
	lda #<actual : \
	sta c64unit_ActualMemoryLocationPointer+1 : \
	lda #>actual : \
	sta c64unit_ActualMemoryLocationPointer+2 : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	ldx #<length : \
	ldy #>length : \
	jsr c64unit_AssertMemoryEqual : \
.)


; @param string customMessage
#define assertNegativeFlagSignedNotSet(customMessage) .(: \
	jsr c64unit_StoreNotNegativeFlagStatusSigned : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param string customMessage
#define assertNegativeFlagSignedSet(customMessage) .(: \
	jsr c64unit_StoreNegativeFlagStatusSigned : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param string customMessage
#define assertNegativeFlagUnsignedNotSet(customMessage) .(: \
	jsr c64unit_StoreNotNegativeFlagStatusUnsigned : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param string customMessage
#define assertNegativeFlagUnsignedSet(customMessage) .(: \
	jsr c64unit_StoreNegativeFlagStatusUnsigned : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param byte expected
; @param byte actual
; @param string customMessage
#define assertNotEqual(expected,actual,customMessage) .(: \
	lda #expected : \
	sta c64unit_expected : \
	lda actual : \
	sta c64unit_actual : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertNotEqual : \
.)


; @param byte expected
; @param string customMessage
#define assertNotEqualToA(expected,customMessage) .(: \
	sta c64unit_actual : \
	lda #expected : \
	sta c64unit_expected : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertNotEqual : \
.)


; @param byte expected
; @param string customMessage
#define assertNotEqualToX(expected,customMessage) .(: \
	stx c64unit_actual : \
	lda #expected : \
	sta c64unit_expected : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertNotEqual : \
.)


; @param byte expected
; @param string customMessage
#define assertNotEqualToY(expected,customMessage) .(: \
	sty c64unit_actual : \
	lda #expected : \
	sta c64unit_expected : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertNotEqual : \
.)


; @param string customMessage
#define assertOverflowFlagNotSet(customMessage) .(: \
	jsr c64unit_StoreNotOverflowFlagStatus : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param string customMessage
#define assertOverflowFlagSet(customMessage) .(: \
	jsr c64unit_StoreOverflowFlagStatus : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param word expected
; @param word actual
; @param string customMessage
#define assertWordEqual(expected,actual,customMessage) .(: \
	ldx #<expected : \
	ldy #>expected : \
	jsr c64unit_SetExpectedImmediateWord : \
	: \
	ldx #<actual : \
	ldy #>actual : \
	jsr c64unit_SetActualAbsoluteWord : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertWordEqual : \
.)


; @param word expected
; @param word actual
; @param string customMessage
#define assertWordNotEqual(expected,actual,customMessage) .(: \
	ldx #<expected : \
	ldy #>expected : \
	jsr c64unit_SetExpectedImmediateWord : \
	: \
	ldx #<actual : \
	ldy #>actual : \
	jsr c64unit_SetActualAbsoluteWord : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertWordNotEqual : \
.)


; @param string customMessage
#define assertZeroFlagNotSet(customMessage) .(: \
	jsr c64unit_StoreNotZeroFlagStatus : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


; @param string customMessage
#define assertZeroFlagSet(customMessage) .(: \
	jsr c64unit_StoreZeroFlagStatus : \
	: \
	jmp textEnd : \
text : \
	.asc customMessage : \
textEnd : \
	ldx #<text : \
	ldy #>text : \
	lda #textEnd - text : \
	beq noCustomMessage : \
	jsr c64unit_PrepareCustomMessageWithConversion : \
noCustomMessage : \
	: \
	jsr c64unit_AssertFlagSet : \
.)


#define c64unitExit() .(: \
	jmp c64unit_ExitWithSuccess : \
.)


; @param byte exitToBasicMode
; @param word screenLocation
#define c64unitRun(exitToBasicMode,screenLocation) .(: \
	.word $0801 : \
	*=$0801 : \
	.word ss,10 : \
	.byt $9e,"2061" : \
	.byt 0 : \
	ss .word 0 : \
	lda #exitToBasicMode : \
	jsr c64unit_InitExitToBasicMode : \
	ldx #<screenLocation : \
	ldy #>screenLocation : \
	jsr c64unit_InitScreenLocation : \
	jsr c64unit_InitScreen : \
.)


; @param word testMethod
#define examineTest(testMethod) .(: \
	jsr c64unit_InitTest : \
	jsr testMethod : \
.)


; @param word dataSet
; @param word output
#define loadDataSet(dataSet,output) .(: \
	ldx #<output : \
	ldy #>output : \
	jsr c64unit_PrepareDataSetOutput : \
	ldx #<dataSet : \
	ldy #>dataSet : \
	jsr c64unit_LoadDataSet : \
.)


; @param word dataSet
#define loadDataSetToA(dataSet) .(: \
	txa : \
	pha : \
	tya : \
	pha : \
	ldx #<dataSet : \
	ldy #>dataSet : \
	jsr c64unit_LoadDataSetToA : \
	sta c64unit_number : \
	pla : \
	tay : \
	pla : \
	tax : \
	lda c64unit_number : \
.)


; @param word dataSet
; @param word output
#define loadDataSetWord(dataSet,output) .(: \
	ldx #<output : \
	ldy #>output : \
	jsr c64unit_PrepareDataSetWordOutput : \
	ldx #<dataSet : \
	ldy #>dataSet : \
	jsr c64unit_LoadDataSetWord : \
.)


; @param word dataSet
; @param word outputLo
; @param word outputHi
#define loadDataSetWordToLoHi(dataSet,outputLo,outputHi) .(: \
	ldx #<outputLo : \
	ldy #>outputLo : \
	jsr c64unit_PrepareDataSetWordLoOutput : \
	ldx #<outputHi : \
	ldy #>outputHi : \
	jsr c64unit_PrepareDataSetWordHiOutput : \
	ldx #<dataSet : \
	ldy #>dataSet : \
	jsr c64unit_LoadDataSetWordToLoHi : \
.)


; @return carry flag
#define isDataSetCompleted() .(: \
	jsr c64unit_IsDataSetCompleted : \
.)


; @param word originalMethod
; @param word mockMethod
#define mockMethod(originalMethod,mockMethod) .(: \
	ldx #<originalMethod : \
	ldy #>originalMethod : \
	jsr c64unit_SetOriginalMethodPointer : \
	: \
	ldx #<mockMethod : \
	ldy #>mockMethod : \
	: \
	jsr c64unit_SetMockMethodPointer : \
	jsr c64unit_MockMethod : \
.)


; @param byte length
#define prepareDataSetLength(length) .(: \
	lda #length : \
	jsr c64unit_PrepareDataSetLength : \
.)


; @param word originalMethod
#define unmockMethod(originalMethod) .(: \
	ldx #<originalMethod : \
	ldy #>originalMethod : \
	jsr c64unit_UnmockMethod : \
.)
