
; This file is automatically generated by Symlinker

#ifdef c64unit_include_definitions
#ifndef c64unit_include_package
#define coreBinaryLocation $B000
#include "symbols.asm"
#include "macros.asm"
#endif
#endif

#ifdef c64unit_include_package
c64unit_continuous_block
* = coreBinaryLocation
.dsb (*-c64unit_continuous_block), 0
* = coreBinaryLocation
.bin 0, 0, "../vendor/c64unit-framework/bin/coreB000.bin"
#endif
