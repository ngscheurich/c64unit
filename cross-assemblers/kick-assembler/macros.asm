
// @param word expected
// @param word actual
// @param string customMessage = ""
.macro assertAbsoluteWordEqual(expected, actual, customMessage) {
	ldx #<expected
	ldy #>expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertWordEqual
}


// @param word expected
// @param word actual
// @param string customMessage = ""
.macro assertAbsoluteWordNotEqual(expected, actual, customMessage) {
	ldx #<expected
	ldy #>expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertWordNotEqual
}


// @param string customMessage = ""
.macro assertCarryFlagNotSet(customMessage) {
	jsr c64unit_StoreNotCarryFlagStatus
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param string customMessage = ""
.macro assertCarryFlagSet(customMessage) {
	jsr c64unit_StoreCarryFlagStatus
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param word expectedDataSet
// @param byte actual
// @param string customMessage = ""
.macro assertDataSetEqual(expectedDataSet, actual, customMessage) {
	setActualAbsoluteValue(actual)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


// @param word expectedDataSet
// @param string customMessage = ""
.macro assertDataSetEqualToA(expectedDataSet, customMessage) {
	sta c64unit_actual
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


// @param word expectedDataSet
// @param string customMessage = ""
.macro assertDataSetEqualToX(expectedDataSet, customMessage) {
	stx c64unit_actual
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


// @param word expectedDataSet
// @param string customMessage = ""
.macro assertDataSetEqualToY(expectedDataSet, customMessage) {
	sty c64unit_actual
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


// @param word expectedDataSet
// @param word actual
// @param string customMessage = ""
.macro assertDataSetWordEqual(expectedDataSet, actual, customMessage) {
	ldx #<expectedDataSet
	ldy #>expectedDataSet
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertWordEqual
}


// @param string customMessage = ""
.macro assertDecimalFlagNotSet(customMessage) {
	jsr c64unit_StoreNotDecimalFlagStatus
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param string customMessage = ""
.macro assertDecimalFlagSet(customMessage) {
	jsr c64unit_StoreDecimalFlagStatus
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param byte expected
// @param byte actual
// @param string customMessage = ""
.macro assertEqual(expected, actual, customMessage) {
	setExpectedImmediateValue(expected)
	setActualAbsoluteValue(actual)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertEqual
}


// @param byte expected
// @param string customMessage = ""
.macro assertEqualToA(expected, customMessage) {
	sta c64unit_actual
	setExpectedImmediateValue(expected)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertEqual
}


// @param byte expected
// @param string customMessage = ""
.macro assertEqualToX(expected, customMessage) {
	stx c64unit_actual
	setExpectedImmediateValue(expected)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertEqual
}


// @param byte expected
// @param string customMessage = ""
.macro assertEqualToY(expected, customMessage) {
	sty c64unit_actual
	setExpectedImmediateValue(expected)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertEqual
}


// @param byte expected
// @param byte actual
// @param string customMessage = ""
.macro assertGreater(expected, actual, customMessage) {
	setExpectedImmediateValue(expected)
	setActualAbsoluteValue(actual)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertGreater
}


// @param byte expected
// @param byte actual
// @param string customMessage = ""
.macro assertGreaterOrEqual(expected, actual, customMessage) {
	setExpectedImmediateValue(expected)
	setActualAbsoluteValue(actual)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertGreaterOrEqual
}


// @param byte expected
// @param byte actual
// @param string customMessage = ""
.macro assertLess(expected, actual, customMessage) {
	setExpectedImmediateValue(expected)
	setActualAbsoluteValue(actual)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertLess
}


// @param word expected
// @param word actual
// @param word length
// @param string customMessage = ""
.macro assertMemoryEqual(expected, actual, length, customMessage) {
	lda #<expected
	sta c64unit_ExpectedMemoryLocationPointer+1
	lda #>expected
	sta c64unit_ExpectedMemoryLocationPointer+2
	
	lda #<actual
	sta c64unit_ActualMemoryLocationPointer+1
	lda #>actual
	sta c64unit_ActualMemoryLocationPointer+2
	
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	
	ldx #<length
	ldy #>length
	jsr c64unit_AssertMemoryEqual
}


// @param string customMessage = ""
.macro assertNegativeFlagSignedNotSet(customMessage) {
	jsr c64unit_StoreNotNegativeFlagStatusSigned
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param string customMessage = ""
.macro assertNegativeFlagSignedSet(customMessage) {
	jsr c64unit_StoreNegativeFlagStatusSigned
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param string customMessage = ""
.macro assertNegativeFlagUnsignedNotSet(customMessage) {
	jsr c64unit_StoreNotNegativeFlagStatusUnsigned
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param string customMessage = ""
.macro assertNegativeFlagUnsignedSet(customMessage) {
	jsr c64unit_StoreNegativeFlagStatusUnsigned
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param byte expected
// @param byte actual
// @param string customMessage = ""
.macro assertNotEqual(expected, actual, customMessage) {
	setExpectedImmediateValue(expected)
	setActualAbsoluteValue(actual)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertNotEqual
}


// @param byte expected
// @param string customMessage = ""
.macro assertNotEqualToA(expected, customMessage) {
	sta c64unit_actual
	setExpectedImmediateValue(expected)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertNotEqual
}


// @param byte expected
// @param string customMessage = ""
.macro assertNotEqualToX(expected, customMessage) {
	stx c64unit_actual
	setExpectedImmediateValue(expected)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertNotEqual
}


// @param byte expected
// @param string customMessage = ""
.macro assertNotEqualToY(expected, customMessage) {
	sty c64unit_actual
	setExpectedImmediateValue(expected)
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertNotEqual
}


// @param string customMessage = ""
.macro assertOverflowFlagNotSet(customMessage) {
	jsr c64unit_StoreNotOverflowFlagStatus
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param string customMessage = ""
.macro assertOverflowFlagSet(customMessage) {
	jsr c64unit_StoreOverflowFlagStatus
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param word expected
// @param word actual
// @param string customMessage = ""
.macro assertWordEqual(expected, actual, customMessage) {
	ldx #<expected
	ldy #>expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertWordEqual
}


// @param word expected
// @param word actual
// @param string customMessage = ""
.macro assertWordNotEqual(expected, actual, customMessage) {
	ldx #<expected
	ldy #>expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<actual
	ldy #>actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertWordNotEqual
}


// @param string customMessage = ""
.macro assertZeroFlagNotSet(customMessage) {
	jsr c64unit_StoreNotZeroFlagStatus
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param string customMessage = ""
.macro assertZeroFlagSet(customMessage) {
	jsr c64unit_StoreZeroFlagStatus
	.if (customMessage != "") {
		prepareCustomMessage(customMessage)
	}
	jsr c64unit_AssertFlagSet
}


// @param byte exitToBasicMode
// @param word screenLocation
.macro c64unit(exitToBasicMode, screenLocation) {

.pc=$0801
BasicUpstart(start)

.pc=$0810
start:
	lda #exitToBasicMode
	jsr c64unit_InitExitToBasicMode
	
	.if (screenLocation == 0) {
		ldx #<$400
		ldy #>$400
	} else {
		ldx #<screenLocation
		ldy #>screenLocation
	}
	jsr c64unit_InitScreenLocation
	
	jsr c64unit_InitScreen
}


.macro c64unitExit() {
	jmp c64unit_ExitWithSuccess
}


// @param word testMethod
.macro examineTest(testMethod) {
	jsr c64unit_InitTest
	jsr testMethod
}


// @param word dataSet
// @param word output
.macro loadDataSet(dataSet, output) {
	ldx #<output
	ldy #>output
	jsr c64unit_PrepareDataSetOutput
	ldx #<dataSet
	ldy #>dataSet
	jsr c64unit_LoadDataSet
}


// @param word dataSet
.macro loadDataSetToA(dataSet) {
	pushXYOnStack()
	ldx #<dataSet
	ldy #>dataSet
	jsr c64unit_LoadDataSetToA
	sta c64unit_number
	pullXYFromStack()
	lda c64unit_number
}


// @param word dataSet
// @param word output
.macro loadDataSetWord(dataSet, output) {
	ldx #<output
	ldy #>output
	jsr c64unit_PrepareDataSetWordOutput
	ldx #<dataSet
	ldy #>dataSet
	jsr c64unit_LoadDataSetWord
}


// @param word dataSet
// @param word outputLo
// @param word outputHi
.macro loadDataSetWordToLoHi(dataSet, outputLo, outputHi) {
	ldx #<outputLo
	ldy #>outputLo
	jsr c64unit_PrepareDataSetWordLoOutput
	ldx #<outputHi
	ldy #>outputHi
	jsr c64unit_PrepareDataSetWordHiOutput
	ldx #<dataSet
	ldy #>dataSet
	jsr c64unit_LoadDataSetWordToLoHi
}


// @return carry flag
.macro isDataSetCompleted() {
	jsr c64unit_IsDataSetCompleted
}


// @param word originalMethod
// @param word mockMethod
.macro mockMethod(originalMethod, mockMethod) {
	ldx #<originalMethod
	ldy #>originalMethod
	jsr c64unit_SetOriginalMethodPointer
	
	ldx #<mockMethod
	ldy #>mockMethod
	jsr c64unit_SetMockMethodPointer
	
	jsr c64unit_MockMethod
}


// @param string customMessage
.macro prepareCustomMessage(customMessage) {

	// Trick to create text label dynamically
	jmp !+
text:
	.text customMessage
!:

	lda #customMessage.size()
	ldx #<text
	ldy #>text
	jsr c64unit_PrepareCustomMessage
}


// @param byte length
.macro prepareDataSetLength(length) {
	lda #length
	jsr c64unit_PrepareDataSetLength
}


.macro pushXYOnStack() {
	txa
	pha
	tya
	pha
}


.macro pullXYFromStack() {
	pla
	tay
	pla
	tax
}


// @access private
// @param byte value
.macro setActualAbsoluteValue(value) {
	lda value
	sta c64unit_actual
}


// @access private
// @param byte value
.macro setExpectedAbsoluteValue(value) {
	lda value
	sta c64unit_expected
}


// @access private
// @param byte value
.macro setExpectedImmediateValue(value) {
	lda #value
	sta c64unit_expected
}


// @param word originalMethod
.macro unmockMethod(originalMethod) {
	ldx #<originalMethod
	ldy #>originalMethod
	jsr c64unit_UnmockMethod
}
