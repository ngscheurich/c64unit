
; @param word expected
; @param word actual
; @param string customMessage = ""
assertAbsoluteWordEqual .macro expected, actual, customMessage = ""
	ldx #<\expected
	ldy #>\expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<\actual
	ldy #>\actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertWordEqual
.endm


; @param word expected
; @param word actual
; @param string customMessage = ""
assertAbsoluteWordNotEqual .macro expected, actual, customMessage = ""
	ldx #<\expected
	ldy #>\expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<\actual
	ldy #>\actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertWordNotEqual
.endm


; @param string customMessage = ""
assertCarryFlagNotSet .macro customMessage = ""
	jsr c64unit_StoreNotCarryFlagStatus
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param string customMessage = ""
assertCarryFlagSet .macro customMessage = ""
	jsr c64unit_StoreCarryFlagStatus
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param word expectedDataSet
; @param byte actual
; @param string customMessage = ""
assertDataSetEqual .macro expectedDataSet, actual, customMessage = ""
	lda \actual
	sta c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	ldx #<\expectedDataSet
	ldy #>\expectedDataSet
	jsr c64unit_AssertDataSetEqual
.endm


; @param word expectedDataSet
; @param string customMessage = ""
assertDataSetEqualToA .macro expectedDataSet, customMessage = ""
	sta c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	ldx #<\expectedDataSet
	ldy #>\expectedDataSet
	jsr c64unit_AssertDataSetEqual
.endm


; @param word expectedDataSet
; @param string customMessage = ""
assertDataSetEqualToX .macro expectedDataSet, customMessage = ""
	stx c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	ldx #<\expectedDataSet
	ldy #>\expectedDataSet
	jsr c64unit_AssertDataSetEqual
.endm


; @param word expectedDataSet
; @param string customMessage = ""
assertDataSetEqualToY .macro expectedDataSet, customMessage = ""
	sty c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	ldx #<\expectedDataSet
	ldy #>\expectedDataSet
	jsr c64unit_AssertDataSetEqual
.endm


; @param word expectedDataSet
; @param word actual
; @param string customMessage = ""
assertDataSetWordEqual .macro expectedDataSet, actual, customMessage = ""
	ldx #<\expectedDataSet
	ldy #>\expectedDataSet
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<\actual
	ldy #>\actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertWordEqual
.endm


; @param string customMessage = ""
assertDecimalFlagNotSet .macro customMessage = ""
	jsr c64unit_StoreNotDecimalFlagStatus
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param string customMessage = ""
assertDecimalFlagSet .macro customMessage = ""
	jsr c64unit_StoreDecimalFlagStatus
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param byte expected
; @param byte actual
; @param string customMessage = ""
assertEqual .macro expected, actual, customMessage = ""
	lda \expected
	sta c64unit_expected
	lda \actual
	sta c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertEqual
.endm


; @param byte expected
; @param string customMessage = ""
assertEqualToA .macro expected, customMessage = ""
	sta c64unit_actual
	lda \expected
	sta c64unit_expected
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertEqual
.endm


; @param byte expected
; @param string customMessage = ""
assertEqualToX .macro expected, customMessage = ""
	stx c64unit_actual
	lda \expected
	sta c64unit_expected
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertEqual
.endm


; @param byte expected
; @param string customMessage = ""
assertEqualToY .macro expected, customMessage = ""
	sty c64unit_actual
	lda \expected
	sta c64unit_expected
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertEqual
.endm


; @param byte expected
; @param byte actual
; @param string customMessage = ""
assertGreater .macro expected, actual, customMessage = ""
	lda \expected
	sta c64unit_expected
	lda \actual
	sta c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertGreater
.endm


; @param byte expected
; @param byte actual
; @param string customMessage = ""
assertGreaterOrEqual .macro expected, actual, customMessage = ""
	lda \expected
	sta c64unit_expected
	lda \actual
	sta c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertGreaterOrEqual
.endm


; @param byte expected
; @param byte actual
; @param string customMessage = ""
assertLess .macro expected, actual, customMessage = ""
	lda \expected
	sta c64unit_expected
	lda \actual
	sta c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertLess
.endm


; @param word expected
; @param word actual
; @param word length
; @param string customMessage = ""
assertMemoryEqual .macro expected, actual, length, customMessage = ""
	lda #<\expected
	sta c64unit_ExpectedMemoryLocationPointer+1
	lda #>\expected
	sta c64unit_ExpectedMemoryLocationPointer+2
	
	lda #<\actual
	sta c64unit_ActualMemoryLocationPointer+1
	lda #>\actual
	sta c64unit_ActualMemoryLocationPointer+2
	
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	
	ldx #<\length
	ldy #>\length
	jsr c64unit_AssertMemoryEqual
.endm


; @param string customMessage = ""
assertNegativeFlagSignedNotSet .macro customMessage = ""
	jsr c64unit_StoreNotNegativeFlagStatusSigned
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param string customMessage = ""
assertNegativeFlagSignedSet .macro customMessage = ""
	jsr c64unit_StoreNegativeFlagStatusSigned
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param string customMessage = ""
assertNegativeFlagUnsignedNotSet .macro customMessage = ""
	jsr c64unit_StoreNotNegativeFlagStatusUnsigned
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param string customMessage = ""
assertNegativeFlagUnsignedSet .macro customMessage = ""
	jsr c64unit_StoreNegativeFlagStatusUnsigned
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param byte expected
; @param byte actual
; @param string customMessage = ""
assertNotEqual .macro expected, actual, customMessage = ""
	lda \expected
	sta c64unit_expected
	lda \actual
	sta c64unit_actual
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertNotEqual
.endm


; @param byte expected
; @param string customMessage = ""
assertNotEqualToA .macro expected, customMessage = ""
	sta c64unit_actual
	lda \expected
	sta c64unit_expected
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertNotEqual
.endm


; @param byte expected
; @param string customMessage = ""
assertNotEqualToX .macro expected, customMessage = ""
	stx c64unit_actual
	lda \expected
	sta c64unit_expected
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertNotEqual
.endm


; @param byte expected
; @param string customMessage = ""
assertNotEqualToY .macro expected, customMessage = ""
	sty c64unit_actual
	lda \expected
	sta c64unit_expected
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertNotEqual
.endm


; @param string customMessage = ""
assertOverflowFlagNotSet .macro customMessage = ""
	jsr c64unit_StoreNotOverflowFlagStatus
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param string customMessage = ""
assertOverflowFlagSet .macro customMessage = ""
	jsr c64unit_StoreOverflowFlagStatus
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param word expected
; @param word actual
; @param string customMessage = ""
assertWordEqual .macro expected, actual, customMessage = ""
	ldx #<\expected
	ldy #>\expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<\actual
	ldy #>\actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertWordEqual
.endm


; @param word expected
; @param word actual
; @param string customMessage = ""
assertWordNotEqual .macro expected, actual, customMessage = ""
	ldx #<\expected
	ldy #>\expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<\actual
	ldy #>\actual
	jsr c64unit_SetActualAbsoluteWord
	
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertWordNotEqual
.endm


; @param string customMessage = ""
assertZeroFlagNotSet .macro customMessage = ""
	jsr c64unit_StoreNotZeroFlagStatus
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param string customMessage = ""
assertZeroFlagSet .macro customMessage = ""
	jsr c64unit_StoreZeroFlagStatus
	.if \customMessage
		prepareCustomMessage \customMessage
	.fi
	jsr c64unit_AssertFlagSet
.endm


; @param byte exitToBasicMode = 1
; @param word screenLocation = $0400
c64unit .macro exitToBasicMode = 1, screenLocation = $0400

*=$0801
	.word ss,10
	.null $9e, format("%d", start)
ss	.word 0

start
	lda #\exitToBasicMode
	jsr c64unit_InitExitToBasicMode
	
	ldx #<\screenLocation
	ldy #>\screenLocation
	jsr c64unit_InitScreenLocation
	
	jsr c64unit_InitScreen
.endm


c64unitExit .macro
	jmp c64unit_ExitWithSuccess
.endm


; @param word testMethod
examineTest .macro testMethod
	jsr c64unit_InitTest
	jsr \testMethod
.endm


; @param word dataSet
; @param word output
loadDataSet .macro dataSet, output
	ldx #<\output
	ldy #>\output
	jsr c64unit_PrepareDataSetOutput
	ldx #<\dataSet
	ldy #>\dataSet
	jsr c64unit_LoadDataSet
.endm


; @param word dataSet
loadDataSetToA .macro dataSet
	pushXYOnStack
	ldx #<\dataSet
	ldy #>\dataSet
	jsr c64unit_LoadDataSetToA
	sta c64unit_number
	pullXYFromStack
	lda c64unit_number
.endm


; @param word dataSet
; @param word output
loadDataSetWord .macro dataSet, output
	ldx #<\output
	ldy #>\output
	jsr c64unit_PrepareDataSetWordOutput
	ldx #<\dataSet
	ldy #>\dataSet
	jsr c64unit_LoadDataSetWord
.endm


; @param word dataSet
; @param word outputLo
; @param word outputHi
loadDataSetWordToLoHi .macro dataSet, outputLo, outputHi
	ldx #<\outputLo
	ldy #>\outputLo
	jsr c64unit_PrepareDataSetWordLoOutput
	ldx #<\outputHi
	ldy #>\outputHi
	jsr c64unit_PrepareDataSetWordHiOutput
	ldx #<\dataSet
	ldy #>\dataSet
	jsr c64unit_LoadDataSetWordToLoHi
.endm


; @return carry flag
isDataSetCompleted .macro
	jsr c64unit_IsDataSetCompleted
.endm


; @param word originalMethod
; @param word mockMethod
mockMethod .macro originalMethod, mockMethod
	ldx #<\originalMethod
	ldy #>\originalMethod
	jsr c64unit_SetOriginalMethodPointer
	
	ldx #<\mockMethod
	ldy #>\mockMethod
	jsr c64unit_SetMockMethodPointer
	
	jsr c64unit_MockMethod
.endm


; @param string customMessage
prepareCustomMessage .macro customMessage
	; Trick to create text label dynamically
	jmp +
	.enc "screen"
text .text \customMessage
	.enc "none"
+
	lda #size(text)
	ldx #<text
	ldy #>text
	jsr c64unit_PrepareCustomMessage
.endm


; @param byte length
prepareDataSetLength .macro length
	lda #\length
	jsr c64unit_PrepareDataSetLength
.endm


pushXYOnStack .macro
	txa
	pha
	tya
	pha
.endm


pullXYFromStack .macro
	pla
	tay
	pla
	tax
.endm


; @param word originalMethod
unmockMethod .macro originalMethod
	ldx #<\originalMethod
	ldy #>\originalMethod
	jsr c64unit_UnmockMethod
.endm
